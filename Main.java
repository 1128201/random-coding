import java.util.Scanner;

public class Main {

        public static void main(String []args) throws InterruptedException {
            
            /** Player Data */
            var playerClass = "no";


            /** Variables/Imports And Stuff */
            Scanner scan = new Scanner(System.in);

            String scanInput = scan.nextLine();

            /** version notice */
            System.out.println("This Is A Development Build, v.0.0.4DPR1");
            Thread.sleep(3000);

            /** Intro Text Secene*/
            System.out.println("██████╗░██╗░░░██╗███╗░░██╗░██████╗░███████╗░█████╗░███╗░░██╗  ░██████╗░██╗░░░██╗███████╗░██████╗████████╗");
                Thread.sleep(500);
            System.out.println("██╔══██╗██║░░░██║████╗░██║██╔════╝░██╔════╝██╔══██╗████╗░██║  ██╔═══██╗██║░░░██║██╔════╝██╔════╝╚══██╔══╝");
                Thread.sleep(500);
            System.out.println("██║░░██║██║░░░██║██╔██╗██║██║░░██╗░█████╗░░██║░░██║██╔██╗██║  ██║██╗██║██║░░░██║█████╗░░╚█████╗░░░░██║░░░");
                Thread.sleep(500);
            System.out.println("██║░░██║██║░░░██║██║╚████║██║░░╚██╗██╔══╝░░██║░░██║██║╚████║  ╚██████╔╝██║░░░██║██╔══╝░░░╚═══██╗░░░██║░░░");
                Thread.sleep(500);
            System.out.println("██████╔╝╚██████╔╝██║░╚███║╚██████╔╝███████╗╚█████╔╝██║░╚███║  ░╚═██╔═╝░╚██████╔╝███████╗██████╔╝░░░██║░░░");
                Thread.sleep(500);
            System.out.println("╚═════╝░░╚═════╝░╚═╝░░╚══╝░╚═════╝░╚══════╝░╚════╝░╚═╝░░╚══╝  ░░░╚═╝░░░░╚═════╝░╚══════╝╚═════╝░░░░╚═╝░░░");
                Thread.sleep(1500);

            /** Main Menu/Class Selection */
                System.out.println("Welcome To Dungeon Quest!");
                    Thread.sleep(500);
                System.out.println("Choose Your Class!");
                    Thread.sleep(500);
                System.out.println("Warrior");
                    Thread.sleep(500);
                System.out.println("Archer");
                    Thread.sleep(500);
                System.out.println("Mage");
                    Thread.sleep(500);
                System.out.println("Who Do You Choose!(Type The Name Of The Class And Press ENTER):");
                while(scanInput == "no") {

                        Thread.sleep(1);

                }
                if(scanInput == "Warrior") {

                    playerClass = "Warrior";
                    System.out.println("You Have Slected Warrior");

                } else if(scanInput == "Archer") {

                    playerClass = "Archer";
                    System.out.println("You Have Selected Archer");

                } else if (scanInput == "Mage") {

                    playerClass = "Mage";
                    System.out.println("You Have Selected Mage");

                } else{

                    System.out.println("Sorry That Is Not An Availible Class(Please Restart The Game As I Need To Make Is To That You Can Retry Choosing Your Class)");


                }

        }

}